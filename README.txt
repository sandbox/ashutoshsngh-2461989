Module for mapping addresses provided by site administrator on google map.

If user shares their current location map will show nearest location based on 
user current location in different marker color.

Default settings:-
"#FE7569": Nearest Marker Color
"#347F90":Other Then Nearest Marker Color
"#EEEA1A":User Current Position
"700px":Map height
"500px":Map width

You can change these colors from module configuration page:-
admin/config/address-mapper/settings

============
Installation
============

1.Install the module like any other drupal module.

2.After installation a new vocabulary will be created which will be used as 
  address mapper vocabulary.

3.Add some terms under "Address Mapper" vocabulary.

4.Description field will be used for writing the full address and term name will
  come after hovering or clicking on any marker on map.

5.Go to blocks section and search for "Address Mapper".

6.Assign "Address Mapper" block to any page based on your requirement.
