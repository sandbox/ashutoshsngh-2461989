<?php

function address_mapper_configuration($form, &$form_state) { 
  $variable = variable_get('nearest_address_color'); 
  $form['address_mapper_color'] = array(
    '#type' => 'fieldset',
    '#title' => t('Manage marker color'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['address_mapper_color']['nearest_address_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Nearest address color'),
    '#description' => t('Marker color for nearest address.'),
    '#required' => TRUE,
    '#default_value' => variable_get('nearest_address_color',''),
  );

  $form['address_mapper_color']['other_address_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Other Address Color'),
    '#description' => t('Marker color for all other addresses.'),
    '#required' => TRUE,
    '#default_value' => variable_get('other_address_color',''),
  );

  $form['address_mapper_color']['user_position_color'] = array(
    '#type' => 'textfield',
    '#title' => t('User Position color'),
    '#description' => t('Marker color for user address.'),
    '#required' => TRUE,
    '#default_value' => variable_get('user_position_color',''),
  );

  $form['map_properties'] = array(
    '#type' => 'fieldset',
    '#title' => t('Map Properties'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['map_properties']['map_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Map width'),
    '#description' => t('Width of map.'),
    '#required' => TRUE,
    '#default_value' => variable_get('map_width',''),
  );

  $form['map_properties']['map_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Map height'),
    '#description' => t('Height of map.'),
    '#required' => TRUE,
    '#default_value' => variable_get('map_height',''),
  );

  return system_settings_form($form);
}
