(function($) {
    Drupal.addressMapper = Drupal.addressMapper || {};
    Drupal.behaviors.addressMapper = {
        attach: function(context, settings) {
            var latlng = new google.maps.LatLng(37.770308, -122.412272);
            var address_count;
            var mapInitialize = function() {
                var myOptions = {
                    zoom: 3,
                    center: latlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    mapTypeControl: false,
                    zoomControl: true,
                    scaleControl: false,
                    scrollwheel: false
                };
                geocoderResponse = new google.maps.Geocoder();
                mapResponse = new google.maps.Map(document.getElementById("map-canvas"), myOptions); //Created map
                var infowindow = new google.maps.InfoWindow();
                var bounds = new google.maps.LatLngBounds();
                mapResponse.fitBounds(bounds);
                zoomChangeBoundsListener =
                        google.maps.event.addListenerOnce(mapResponse, 'bounds_changed', function(event) {
                            if (this.getZoom()) {
                                this.setZoom(10);
                            }
                        });
                setTimeout(function() {
                    google.maps.event.removeListener(zoomChangeBoundsListener)
                }, 2000);
            };
            var locations = [];
            var markaddressonmap = function() {
                if (geocoderResponse) {
                    var addresses = $.parseJSON(Drupal.settings.address);                    
                    address_count = addresses.length;
                    jQuery(addresses).each(function(key, value) {
                        geocoderResponse.geocode({'address': value.address}, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                locations.push({lat: results[0].geometry.location.lat(), lon: results[0].geometry.location.lng(), place: value.address,name:value.name});
                                if (locations.length == address_count) {
                                    init(locations);
                                }

                            }
                        });
                    });
                }
            };

            var finalarray = [];
            var userlocation = [];
            var init = function(locations) {                
                $(locations).each(function(key, value) {
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function(position) {                            
                            var dist = haversine(value.lat, value.lon, position.coords.latitude, position.coords.longitude);
                            finalarray.push({'key': dist, 'data': value});
                            userlocation.push({'lat': position.coords.latitude, 'lon': position.coords.longitude});
                            finaldata(finalarray, position.coords.latitude, position.coords.longitude);
                        });
                    }
                });
            };

            var finaldata = function(finalarray, userlat, userlon) {
                if (finalarray.length == address_count) {
                    addressMapper(finalarray, userlat, userlon);
                }
            };

            var addressMapper = function(alladdress, userlat, userlon) {                
                alladdress.sort(function(a, b) {
                    return a.key - b.key;
                });
                var i = 1;
                jQuery.each(alladdress, function(key, value) {
                    var latlng = new google.maps.LatLng(value.data.lat, value.data.lon);
                    if (i == 1) {
                        mapResponse.setCenter(latlng);
                        var marker = new google.maps.Marker({
                            map: mapResponse,
                            position: latlng,
                            icon: markerColor(Drupal.settings.map_default.nearest_address_color)
                        });
                        mapInfoWindow(marker, value.data.name + '<br/>' + value.data.place);
                    } else {
                        var marker = new google.maps.Marker({
                            map: mapResponse,
                            position: latlng,
                            icon: markerColor(Drupal.settings.map_default.other_address_color)
                        });
                        mapInfoWindow(marker, value.data.name + '<br/>' + value.data.place);
                    }
                    i++;
                });
                markuserposition(userlat, userlon);
            };

            var markuserposition = function(userlat, userlon) {
                var userlatlng = new google.maps.LatLng(userlat, userlon);
                geocoderResponse.geocode({'latLng': userlatlng}, function(results, status) {                    
                    if (status == google.maps.GeocoderStatus.OK) {
                        var marker = new google.maps.Marker({
                            map: mapResponse,
                            position: userlatlng,
                            title: results[0].formatted_address,
                            icon: markerColor(Drupal.settings.map_default.user_position_color)
                        });
                        mapInfoWindow(marker, 'My Location <br/>' + results[0].formatted_address);
                    }
                });

            };

            var mapInfoWindow = function(marker, infocontent) {
                marker['infowindow'] = new google.maps.InfoWindow({
                    content: '<div class="scrollFix">' + infocontent + '</div>'
                });
                google.maps.event.addListener(marker, 'click', function() {
                    this['infowindow'].open(mapResponse, this);
                });
            };

            var markerColor = function(pinColor) {
                var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
                        new google.maps.Size(21, 34),
                        new google.maps.Point(0, 0),
                        new google.maps.Point(10, 34));
                return pinImage;
            };

            var haversine = function(lat1, lon1, lat2, lon2) {
                var R = 6372.8; // Earth Radius in Kilometers

                var dLat = Deg2Rad(lat2 - lat1);
                var dLon = Deg2Rad(lon2 - lon1);
                var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                        Math.cos(Deg2Rad(lat1)) * Math.cos(Deg2Rad(lat2)) *
                        Math.sin(dLon / 2) * Math.sin(dLon / 2);
                var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                var d = R * c;
                // Return Distance in Kilometers
                return d;
            };
            var Deg2Rad = function(deg) {
                return deg * Math.PI / 180;
            };
            mapInitialize();
            markaddressonmap();
        }
    };
})(jQuery);